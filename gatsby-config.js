module.exports = {
  siteMetadata: {
    name: "Vlinder",
    title: "Vlinder",
    description: "Manage your finances. Know your footprint. Make a change.",
    author: "Atoma Solutions GmbH",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `vlinder`,
        short_name: `vlinder`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-zeit-now`,
      options: {
        globalHeaders: {
          "referrer-policy": "same-origin",
          "feature-policy":
            "geolocation 'self'; microphone 'self'; camera 'self'",
          "expect-ct": "max-age=604800, enforce",
          "strict-transport-security": "max-age=31536000; includeSubDomains",
          "x-frame-options": "DENY",
          "x-xss-protection": "1; mode=block",
          "x-content-type-options": "nosniff",
          "x-download-options": "noopen",
        },
      },
    },
    {
      resolve: "gatsby-plugin-mailchimp",
      options: {
        endpoint:
          "https://app.us4.list-manage.com/subscribe/post?u=6191fbcdbe20fb876c4a9d903&amp;id=e5ebec339d",
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
