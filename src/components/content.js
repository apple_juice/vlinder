import React from "react"

export const Title = ({ type, text }) => {
  return type === "lead" ? <h1>{text}</h1> : <h3>{text}</h3>
}

export const Text = ({ data }) =>
  data.map((item, i) => {
    switch (item.type) {
      case "h2":
        return <h2 key={i + item.text}>{item.text}</h2>
      case "h3":
        return <h3 key={i + item.text}>{item.text}</h3>
      case "h4":
        return <h4 key={i + item.text}>{item.text}</h4>
      case "ul":
        return (
          <ul key={i + item.text}>
            {item.text.map((row, j) => (
              <li key={j + row}>{row}</li>
            ))}
          </ul>
        )
      case "ol":
        return (
          <ol key={i + item.text}>
            {item.text.map((row, j) => (
              <li key={j + row}>{row}</li>
            ))}
          </ol>
        )
      case "lead":
        return (
          <React.Fragment key={i + item.text}>
            {item.text.map(row => (
              <p key={row} className="lead">
                {row}
              </p>
            ))}
          </React.Fragment>
        )
      default:
        return (
          <React.Fragment key={i + item.text}>
            {item.text.map((row, j) => (
              <p key={j + row}>{row}</p>
            ))}
          </React.Fragment>
        )
    }
  })
