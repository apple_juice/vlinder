import React from "react"
import addToMailchimp from "gatsby-plugin-mailchimp"
import { Row, Col, Form, FormGroup, Input, Label, Button } from "reactstrap"

export default class SubscribeForm extends React.Component {
  state = {
    email: null,
    isSubmitted: false,
    isSubmitting: false,
    errorText: "",
  }

  handleChange = e => {
    this.setState({
      [`${e.target.name}`]: e.target.value,
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    // console.log("submit", this.state)
    this.setState({ isSubmitting: true })

    addToMailchimp(this.state.email, this.state)
      .then(({ msg, result }) => {
        // console.log("msg", `${result}: ${msg}`)
        this.setState({ isSubmitting: false })

        if (result === "error") {
          this.setState({ errorText: msg })
        } else {
          this.setState({ isSubmitted: true })
        }
      })
      .catch(err => {
        console.log("err", err)
        this.setState({ errorText: err })
      })
  }

  render() {
    const { isSubmitting, isSubmitted, errorText } = this.state

    return (
      <Form
        className="form_subscribe validate"
        method="post"
        id="mc-embedded-subscribe-form"
        name="mc-embedded-subscribe-form"
        target="_blank"
        onSubmit={this.handleSubmit}
        noValidate
      >
        <FormGroup>
          <h4>{isSubmitted ? "Thank you!" : "Take part in beta testing"}</h4>
          <Label htmlFor="email" className="control-label">
            {isSubmitted
              ? "You will receive a link to download the Vlinder app soon."
              : "Please enter your email and operating system of your phone to be among the first Vlinder users."}
          </Label>
        </FormGroup>

        {!isSubmitted && (
          <>
            <div id="mc_embed_signup">
              <div id="mc_embed_signup_scroll">
                <FormGroup className="mc-field-group">
                  <Input
                    className="required email"
                    type="email"
                    placeholder="Email"
                    name="email"
                    onChange={this.handleChange}
                  />
                  {errorText !== "" && (
                    <div
                      className="form-error"
                      dangerouslySetInnerHTML={{ __html: errorText }}
                    />
                  )}
                </FormGroup>
                <FormGroup className="mc-field-group">
                  <Row>
                    <Col>
                      <FormGroup>
                        <Label
                          for="mce-group[69484]-69484-0"
                          check
                          className="control-label control-label--input form-control"
                        >
                          <Input
                            type="checkbox"
                            value="1"
                            name="group[69484][1]"
                            id="mce-group[69484]-69484-0"
                            onChange={this.handleChange}
                          />{" "}
                          iOS
                        </Label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <Label
                          for="mce-group[69484]-69484-1"
                          check
                          className="control-label control-label--input form-control"
                        >
                          <Input
                            type="checkbox"
                            value="2"
                            name="group[69484][2]"
                            id="mce-group[69484]-69484-1"
                            onChange={this.handleChange}
                          />{" "}
                          Android
                        </Label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <Button
                        type="submit"
                        color="primary"
                        name="subscribe"
                        id="mc-embedded-subscribe"
                        className="button"
                        block
                        disabled={isSubmitting}
                      >
                        Subscribe
                      </Button>
                    </Col>
                  </Row>
                </FormGroup>

                <div
                  style={{ position: "absolute", left: "-5000px" }}
                  aria-hidden="true"
                >
                  <input
                    type="text"
                    name="b_6191fbcdbe20fb876c4a9d903_e5ebec339d"
                    tabIndex="-1"
                    value=""
                  />
                </div>
              </div>
            </div>
          </>
        )}
      </Form>
    )
  }
}
