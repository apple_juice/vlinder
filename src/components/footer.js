import React from "react"
import PropTypes from "prop-types"
import { Container, Row, Col } from "reactstrap"

import { CURRENT_YEAR } from "../utils/constants"

const Footer = ({ siteTitle, author }) => (
  <footer className="footer">
    <Container>
      <Row className="justify-content-between align-items-center">
        <Col xs={12} sm={6} className="text-center text-sm-left">
          <div className="footer-brand">{siteTitle}</div>
        </Col>
        <Col xs={12} sm={6} className="text-center text-sm-right">
          &copy; {CURRENT_YEAR} {author}
        </Col>
      </Row>
    </Container>
  </footer>
)

Footer.propTypes = {
  siteTitle: PropTypes.string,
  author: PropTypes.string,
}

Footer.defaultProps = {
  siteTitle: ``,
  author: ``,
}

export default Footer
