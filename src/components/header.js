import React, { useState } from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap"

import logo from "../images/logo.svg"

const Header = props => {
  const { siteTitle } = props
  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => setIsOpen(!isOpen)

  return (
    <header className={isOpen ? "header open" : "header"}>
      <Navbar expand="md">
        <Container>
          <Link to="/">
            <img src={logo} alt={siteTitle} width="142px" />
          </Link>
          <NavbarToggler onClick={toggle} className={isOpen ? "active" : ""} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to="/about" className="nav-link" activeClassName="active">
                  About
                </Link>
              </NavItem>
              <NavItem>
                <Link
                  to="/privacy"
                  className="nav-link"
                  activeClassName="active"
                >
                  Security &amp; privacy
                </Link>
              </NavItem>
              <NavItem>
                <Link
                  to="/contacts"
                  className="nav-link"
                  activeClassName="active"
                >
                  Contacts
                </Link>
              </NavItem>
              <NavItem>
                <NavLink
                  href="https://twitter.com/vlinderapp"
                  className="nav-link--icon"
                >
                  <i className="icon icon--twitter" />
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

Navbar.propTypes = {
  light: PropTypes.bool,
  dark: PropTypes.bool,
  fixed: PropTypes.string,
  color: PropTypes.string,
  role: PropTypes.string,
  expand: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
}

NavbarBrand.propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
}

NavbarText.propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  // pass in custom element to use
}

export default Header
