import React, { Component } from "react"
import { Container, Row, Col } from "reactstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"

import { EMAIL } from "../utils/constants"

class AboutPage extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33,
    },
    zoom: 11,
  }

  render() {
    return (
      <Layout>
        <SEO title="Contacts" />

        <section className="section section_contacts">
          <Container>
            <h1>Contacts</h1>

            <div className="section_contacts__map">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2686.058611591604!2d13.088897215633201!3d47.683281679189854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477694fa2f092cef%3A0x42d84ceeaf1c25af!2zV2ljaHRsaHViZXJzdHJhw59lIDE2L1RvcCAxLCA1NDAwIEhhbGxlaW4sINCQ0LLRgdGC0YDQuNGP!5e0!3m2!1sru!2sde!4v1587112865229!5m2!1sru!2sde"
                width="600"
                height="450"
                frameborder="0"
                style={{ border: 0 }}
                allowFullscreen=""
                aria-hidden="false"
                title="map"
              ></iframe>
            </div>

            <div className="section_contacts__address">
              <Row>
                <Col>
                  <div className="control-label">Mailing address</div>
                  <p>
                    Wichtlhuberstraße 16/Top 1<br /> 5400, Hallein
                    <br /> Austria
                  </p>
                </Col>
                <Col>
                  <div className="control-label">Email</div>
                  <p>
                    <a href={`mailto:${EMAIL}`}>{EMAIL}</a>
                  </p>
                </Col>
              </Row>
            </div>
          </Container>
        </section>
      </Layout>
    )
  }
}

export default AboutPage
