import React from "react"
import { Container, Row, Col } from "reactstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"

import { Title, Text } from "../components/content"

const pageData = [
  {
    type: "lead",
    title: "About",
    content: [
      {
        type: "lead",
        text: [
          "Vlinder means butterfly in Dutch. We believe in the butterfly effect of a small behavioral change in a connected world. This is why we created Vlinder, a digital participatory platform that empowers people to make changes in their consumption habits and contribute to impact projects.",
        ],
      },
      {
        type: "h4",
        text: ["Vlinder mission"],
      },
      {
        type: "paragraph",
        text: [
          "Vlinder mission is to make people happier by providing a new perspective on their lifestyle and empowering them to act on climate change.",
        ],
      },
    ],
  },
  {
    title: "Manage your finances",
    content: [
      {
        type: "ul",
        text: [
          "Connect all your bank accounts",
          "Add your crypto accounts",
          "Scan your checks to add cash expenses",
          "Get a helicopter view of your finances and lifestyle",
          "Send money to friends and neighbors, split payments",
          "Tie the VlinderCard to any bank account or crypto wallet ",
        ],
      },
    ],
  },
  {
    title: "Know your footprint",
    content: [
      {
        type: "ul",
        text: [
          "Analyze your bank accounts and cash expenses to know your footprint",
          "Track your crypto investment footprint",
          "Benchmark with others",
          "Set sustainable financial and personal goals",
        ],
      },
    ],
  },
  {
    title: "Make a change",
    content: [
      {
        type: "ul",
        text: [
          "Discover local community initiatives, impact projects, and green financial products",
          "Offset your footprint by investing into green financial products",
          "Support UN SDG goals by contributing to impact projects",
          "Discover and initiate local impact initiatives: coordinate with your neighbors, crowdfund, co-create",
          "Make your VlinderCard cash back work for sustainability projects",
        ],
      },
    ],
  },
]

const AboutPage = () => (
  <Layout>
    <SEO title="About" />

    <section className="section section_about">
      <Container>
        {pageData.map((item, i) => {
          return (
            <div className="section_about__group" key={i}>
              <Row>
                <Col xs={12} sm={4} md={5}>
                  <Title type={item.type} text={item.title} />
                </Col>
                <Col xs={12} sm={8} md={7} className="px-md-0">
                  <Text data={item.content} />
                </Col>
              </Row>
            </div>
          )
        })}
      </Container>
    </section>
  </Layout>
)

export default AboutPage
