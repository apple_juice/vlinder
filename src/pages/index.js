import React, { useState } from "react"
import {
  Container,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap"
import { useMediaQuery } from "react-responsive"

import Layout from "../components/layout"
import SEO from "../components/seo"
import FormSubscribe from "../components/form"

import leadImage from "../images/lead-image.png"
import leadVideoPoster from "../images/lead-video.jpg"
import leadVideoMP4 from "../images/lead-video.mp4"
import leadVideoWEBM from "../images/lead-video.webm"
import leadVideoOGV from "../images/lead-video.ogv"

export default function IndexPage() {
  const isMobile = useMediaQuery({ maxWidth: 575 })

  const Video = () => (
    <div className="video_container">
      <video
        controls={isMobile}
        autoPlay={!isMobile}
        loop={!isMobile}
        muted
        poster={leadVideoPoster}
      >
        <source src={leadVideoMP4} type="video/mp4" />
        <source src={leadVideoWEBM} type="video/webm" />
        <source src={leadVideoOGV} type="video/ogg" />
      </video>
    </div>
  )

  const [modal, setModal] = useState(false)

  const toggle = () => setModal(!modal)

  return (
    <Layout>
      <SEO title="Home" />

      <section className="section section_lead">
        <Container>
          <Row className="flex-sm-row-reverse align-items-sm-center justify-content-sm-center">
            <Col xs={12} sm={7} md={6} xl={6}>
              <h1>
                Manage your finances. <br />
                Know your footprint. <br />
                Make a change.
              </h1>
              {isMobile && (
                <div className="mobile_video">
                  <Button
                    color="secondary"
                    type="button"
                    outline
                    onClick={toggle}
                  >
                    Watch video
                  </Button>
                  <Modal isOpen={modal} toggle={toggle}>
                    <ModalHeader toggle={toggle} />
                    <ModalBody>
                      <Video />
                    </ModalBody>
                  </Modal>
                </div>
              )}
              <FormSubscribe />
            </Col>
            <Col xs={12} sm={5} xl={6} className="text-center">
              {!isMobile && (
                <div className="section_lead__video">
                  <img
                    src={leadImage}
                    width="315px"
                    alt="lead"
                    className="section_lead__image"
                  />
                  <Video />
                </div>
              )}
            </Col>
          </Row>
        </Container>
      </section>
    </Layout>
  )
}
