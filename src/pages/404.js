import React from "react"
import { Container, Row, Col } from "reactstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />

    <section className="section section_lead section_error">
      <Container>
        <Row className="align-items-center text-center">
          <Col>
            <h1>404</h1>
            <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
          </Col>
        </Row>
      </Container>
    </section>
  </Layout>
)

export default NotFoundPage
