import React from "react"
import { Container, Row, Col } from "reactstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"

const phases = [
  {
    checked: true,
    deadline: "Mar 2020",
    list: [
      "Aggregated view of user’s bank accounts",
      "Analyzing and categorizing payments",
      "Carbon footprint monitoring",
    ],
  },
  {
    checked: false,
    deadline: "Dec 2020",
    list: [
      "Impact projects discovery",
      "SEPA payments initiation",
      "Cryptocurrency wallets support",
    ],
  },
  {
    checked: false,
    deadline: "Mar 2021",
    list: [
      "Green financial products marketplace",
      "Personal financial goals tracking",
      "Vlinder card tied to any bank or crypto account",
    ],
  },
  {
    checked: false,
    deadline: "Dec 2021",
    list: ["Community projects governance", "E-commerce/C2B payments"],
  },
]

const listItem = data => {
  let items = []
  for (var i = 0; i < data.length; i++) {
    items.push(<li key={i}>{data[i]}</li>)
  }
  return items
}

const RoadmapPage = () => (
  <Layout>
    <SEO title="Roadmap" />

    <section className="section section_roadmap">
      <Container>
        <h1>Roadmap</h1>

        <div className="section__content">
          <Row className="section_roadmap__group roadmap">
            {phases.map((item, i) => (
              <Col key={i}>
                <div className="roadmap_item">
                  <div className="roadmap_item__header">
                    <h3 className="roadmap_item__title">
                      Phase {i + 1}{" "}
                      {item.checked && <i className="icon icon--check" />}
                    </h3>
                    <div className="roadmap_item__desc">{item.deadline}</div>
                  </div>
                  {item.list && (
                    <ul className="roadmap_item__list">
                      {listItem(item.list)}
                    </ul>
                  )}
                </div>
              </Col>
            ))}
          </Row>
        </div>
      </Container>
    </section>
  </Layout>
)

export default RoadmapPage
