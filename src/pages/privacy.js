import React from "react"
import { Container, Row, Col } from "reactstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { Title, Text } from "../components/content"

import { EMAIL } from "../utils/constants"

const pageData = [
  {
    type: "lead",
    title: (
      <>
        Security &amp; <br /> Privacy
      </>
    ),
    content: [
      {
        type: "lead",
        text: [
          "At Vlinder we care about your privacy and want you to be confident in the way we use your personal information and other data. The information we collect helps us provide the best service possible to you. Before using Vlinder, please familiarize yourselves with the following terms. By accessing or using Vlinder’s services, you are agreeing to be bound by them.",
        ],
      },
    ],
  },
  {
    title: "How we use your data",
    content: [
      {
        type: "paragraph",
        text: [
          "The information you provide will be used by Atoma Solutions GmbH, the developer of Vlinder.",
          "We use the personal information you give us to provide you with the products and services you’d like to use as well as offer our support services. We also use it to help protect you and our services from crime. We may share your information with selected third parties but always make sure it remains protected. We may use your data for our internal marketing analytics purposes to improve our services.",
          "We will collect, use, store, and otherwise process information as permitted by applicable laws.",
          "We don’t sell or make available your data to any third parties for marketing, advertising or related purposes.",
        ],
      },
    ],
  },
  {
    title: (
      <>
        We keep your <br /> information safe
      </>
    ),
    content: [
      {
        type: "paragraph",
        text: [
          "All our data is held in secure servers located in the EEA. The data we store, how we store it and third party applications we use are subject to strict security measures and legal obligations.",
          "We use a number of technical and procedural measures to ensure the safety of your data. These include encrypting data, only storing and transferring it where we know it is safe and restricting access to who can see or use the data.",
        ],
      },
    ],
  },
  {
    title: "You own your data",
    content: [
      {
        type: "paragraph",
        text: [
          "You have various rights such as the right to ask us what information we hold about you, ask us to correct or delete information about you or to restrict our use of it. And you have the right to complain about our use of your data and submit a complaint to the relevant data protection authority.",
          <>
            If you have any questions, comments or complaints regarding how we
            collect, use and store information about you, please send an e-mail
            to <a href={`mailto:${EMAIL}`}>{EMAIL}</a>.
          </>,
        ],
      },
    ],
  },
]

const AboutPage = () => (
  <Layout>
    <SEO title="About" />

    <section className="section section_about">
      <Container>
        {pageData.map((item, i) => {
          return (
            <div className="section_about__group" key={i}>
              <Row>
                <Col xs={12} sm={4} md={5}>
                  <Title type={item.type} text={item.title} />
                </Col>
                <Col xs={12} sm={8} md={7} className="px-md-0">
                  <Text data={item.content} />
                </Col>
              </Row>
            </div>
          )
        })}
      </Container>
    </section>
  </Layout>
)

export default AboutPage
